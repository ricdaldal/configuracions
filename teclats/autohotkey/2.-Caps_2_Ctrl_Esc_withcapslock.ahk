﻿#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
; #Warn  ; Enable warnings to assist with detecting common errors.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.
; Author: 	fwompner gmail com
; Modified by: 	ricdaldal gmail com


; Press					Generates
;--------------------------------
; Shift - CapsLock		CapsLock
; CapsLock				Esc
; CapsLock - key		Ctrl - key
; Esc					Esc


+CapsLock::
;use global variable to keep track of state
if CapsOn = false
{
 CapsOn = true
 SetCapsLockState, on
}
else
{
 CapsOn = false
 SetCapsLockState, off
}
return 

#InstallKeybdHook
SetCapsLockState, alwaysoff
Capslock::
Send {LControl Down}
KeyWait, CapsLock
Send {LControl Up}
if ( A_PriorKey = "CapsLock" )
{
	Send {Esc}
}
