@REM ##############################################################
@REM #
@REM #  WINDOWS BAT
@REM #
@REM ##############################################################
@REM #  Autor          : enric
@REM #  Data           : 11/09/2016
@REM #  Ultima edicio  : 11/09/2016, enric
@REM #  Descripcio     : Posa en funcionament el autohotkey i carrega un layout
@REM ##############################################################
@REM #  Proposit:
@REM #   - Tindre en Windows un layout de teclat com el del Pok3er
@REM #  Requeriments:
@REM #   - Windows i autohotkey
@REM #  Execucio:
@REM #   - Una vegada
@REM #  Notes:
@REM #   - Hi ha un enllac comentat per a descarregar autohotkey
@REM ##############################################################


@REM #####################
@REM ### PREINSTALACIO ###
@REM #####################

@echo off

echo "Benvingut al configurador de Windows"

@REM echo "Descarrega AutoHotkey"
@REM @start http://www.autohotkey.com/

echo "Movent scripts d'AutoHotkey"
@xcopy teclats\autohotkey\* "%homedrive%%homepath%\autohotkey\" /s /e /y /q

echo "Engegant script per defecte"
@start "%homedrive%%homepath%"\autohotkey\defecte.ahk

@echo on
