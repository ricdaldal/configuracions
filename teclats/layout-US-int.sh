##############################################################
#
#  SCRIPT BASH
#
##############################################################
#!/bin/bash
#  Autor          : enric
#  Data           : 11/09/2016
#  Ultima edicio  : 11/09/2016, enric
#  Descripcio     : Configura el layout del teclat a US international amb shift both capslock. 
##############################################################
#  Proposit:
#   - Poder treballar sempre amb el mateix layout (US international) en tots els Linux. 
#   - Canvia la manera de posar el CAPS LOCK. Ara es fa apretant els dos SHIFT al mateix temps.
#  Requeriments:
#   - xkb
#  Execucio:
#   - Com a usuari, una vegada cada sessió.
#  Notes:
#   - La configuració desapareix en cada reinici.
##############################################################

setxkbmap  -layout us -variant intl -option shift:both_capslock

