@REM ##############################################################
@REM #
@REM #  WINDOWS BAT
@REM #
@REM ##############################################################
@REM #  Autor          : enric
@REM #  Data           : 11/09/2016
@REM #  Ultima edicio  : 11/09/2016, enric
@REM #  Descripcio     : Configurar un windows per a treballa
@REM ##############################################################
@REM #  Proposit:
@REM #   - Treballar de la mateixa manera amb tots els windows
@REM #  Requeriments:
@REM #   - 
@REM #  Execucio:
@REM #   - Una vegada
@REM #  Notes:
@REM #   - Este script no fa res. Només crida als scripts escollits.
@REM #   - Els CALL abans de cada crida a un bat son obligatoris en XP. Si no estan, el programa acaba al primer bat.
@REM ##############################################################

@echo off
CALL vim\vim-conf.bat
CALL teclats\autohotkey\autohotkey-conf.bat
@echo on
