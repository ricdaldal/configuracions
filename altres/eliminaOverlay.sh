##############################################################
#
#  SCRIPT BASH
#
##############################################################
#!/bin/bash
#  Autor          : enric
#  Data           : 16/06/2016
#  Ultima edicio  : 16/06/2016, enric
#  Descripcio     : Elimina l'overlay de les scrollbar
##############################################################
#  Proposit:
#   - Fa que les finestres tinguen una scrollbar classica en l'escriptori Unity
#  Requeriments:
#   - Gnome i Unity
#  Execucio:
#   - Com a usuari
#  Notes:
#   - 
##############################################################

gsettings set org.gnome.desktop.interface ubuntu-overlay-scrollbars false

 
