##############################################################
#
#  SCRIPT BASH
#
##############################################################
#!/bin/bash
#  Autor          : enric
#  Data           : 12/09/2016
#  Ultima edicio  : 12/09/2016, enric
#  Descripcio     : Configura l'editor Pluma per a tenir resaltat amb Markdown
##############################################################
#  Proposit:
#   - Tenir el codi Markdown amb colors, resaltatn la seva sintaxi
#  Requeriments:
#   - Editor Pluma. Per defecte en Ubuntu Mate.
#  Execucio:
#   - Com a root, una sola vegada.
#  Notes:
#   - Pluma gasta una biblioteca per a resaltar codi (gtk source view) 
#     La versió 2 de la biblioteca no sap res de markdown, pero sí la 3. 
#     El problema està en que Pluma només mira la 2. 
#     Per tant, instalem la 3 i fem enllaç simbolic des de la carpeta de la 2.
##############################################################

##################
### INSTALACIO ###
##################
sudo apt-get install libgtksourceview-3.0-common

####################
### CONFIGURACIO ###
####################
sudo ln -s /usr/share/gtksourceview-3.0/language-specs/markdown.lang /usr/share/gtksourceview-2.0/language-specs/markdown.lang
