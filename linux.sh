##############################################################
#
#  LINUX SCRIPT	
#
##############################################################
#  Autor          : enric
#  Data           : 11/09/2016
#  Ultima edicio  : 19/09/2016, enric, Afegeix xmodmap
#  Descripcio     : Configurar un linux per a treballar
##############################################################
#  Proposit:
#   - Treballar de la mateixa manera amb tots els linux
#  Requeriments:
#   - 
#  Execucio:
#   - Una vegada
#  Notes:
#   - Totes les crides a altres fitxers son sempre des del directori arrel de configuracions
#   - De vegades, les configuracions de teclat se flipen un poc. Repetir.
##############################################################


sh teclats/layout-US-int.sh
xmodmap teclats/xmodmap/pok3r-like.xmodmap
