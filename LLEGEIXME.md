### Descripcio
Hi ha un arxiu **linux.sh** que posa la configuració al Linux en que s'executa.
Idem per a **windows.bat** però amb windows.

### PROBLEMATICA
Quan un script crida a un altre, este ultim te el working directori del primer (mala explicació, ho sé)
Tots els scripts que tenen sentencies de còpia, o similar, estan pensats per a ser executats des d'aquesta carpeta.

#Vim
  Hi ha scripts per a configurar el programa vim. Linux i Windows.

#Teclats
  Hi ha scripts per a configurar els layouts dels teclats. Linux i Windows

#Teclats - Autohotkey
  Descarrega i configura el layout de teclat en Windows amb Autohotkey

#Altres
  Scripts i arxius de configuracio que no corresponen a cap altra categoria.
