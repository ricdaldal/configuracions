@REM ##############################################################
@REM #
@REM #  WINDOWS BAT
@REM #
@REM ##############################################################
@REM #  Autor          : enric
@REM #  Data           : 11/09/2016
@REM #  Ultima edicio  : 11/09/2016, enric
@REM #  Descripcio     : Configura el vim instal.lat amb els estils meus
@REM ##############################################################
@REM #  Proposit:
@REM #   - Treballar de la mateixa manera amb tots els vim 
@REM #  Requeriments:
@REM #   - vim
@REM #  Execucio:
@REM #   - Com a usuari, una vegada.
@REM #  Notes:
@REM #   - Hi ha unes linies comentades que fan que es baixe (no instale) el vim
@REM ##############################################################

@echo off

@REM echo "Descarrega Vim"
@REM @start http://www.vim.org/download.php

echo "Movent configuracions de vim i vrapper"
@xcopy vim\* "%homedrive%%homepath%\" /s /e /y /q

@echo on
