""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"
"								CONFIGURACIO VIM 			
"
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Instalar gVim
"
" Windows
" 	Directori HOME sera c:\users\usuari
" 	Configurar el teclat amb AutoHotkey canviant CapsLock a Esc amb algun
" 	script
" Linux
" 	Directori HOME sera /home/usuari
" 	Configurar el teclat canviant CapsLock a Esc		
" 
" Posar l'estructura de la carpeta vimfiles en 		c:\users\usuari
" Posar este _vimrc en 								c:\users\usuari
" Posar PATHOGEN en 								c:\users\usuari\vimfiles\autoload
" Posar cada plugin nou en 							c:\users\usuari\vimfiles\bundle
" Cada plugin en bundle te l'estructura vimfiles que sobrescriu
" Aixi, el plugin solarized, que sobrescriu un arxiu de colors
" Te una estructura com esta						c:\users\usuari\vimfiles\bundle\vim-colors-solarized\colors\solarized.vim

"""""""""""""""""""""""""""      BASIC		""""""""""""""""""""""""""""""""""""""""""""""""""""""
set nocompatible							" Desactiva coses especifiques d'algunes distros
set backspace=indent,eol,start				" Permet backspace sobre finals de linia i altres codis especials
set mouse=a									" Ratoli per a tots els modes

"""""""""""""""""""""""""""     PLUGINS		""""""""""""""""""""""""""""""""""""""""""""""""""""""
execute pathogen#infect()					

"""""""""""""""""""""""""""     UNICODE 	""""""""""""""""""""""""""""""""""""""""""""""""""""""
if has("multi_byte")
  if &termencoding == ""
    let &termencoding = &encoding
  endif
  set encoding=utf-8
  setglobal fileencoding=utf-8
  "setglobal bomb
  set fileencodings=ucs-bom,utf-8,latin1
endif

""""""""""""""""""""""""""""""""    TABS 	""""""""""""""""""""""""""""""""""""""""""""""""""""""
" SOFT TABS - se transformen en espais
" set tabstop=4
" set softtabstop=4
" set expandtab 
" HARD TABS
set shiftwidth=4
set tabstop=4

""""""""""""""""""""""""""""""""    COLORS	""""""""""""""""""""""""""""""""""""""""""""""""""""""
syntax enable
nnoremap gV `[v`]							" Resalta l'ultim text insertat

if has('gui_running')
	set background=light
	colorscheme solarized
else
	colorscheme blue						" Quan sengega desde terminal posem un tema basic 
											" perque el Solarized es veu fatal
endif 

""""""""""""""""""""""""""""""""	FONTS   """"""""""""""""""""""""""""""""""""""""""""""	
if has("gui_running")						" Canviem la font perque la que hi havia feia que les
  set guifont=Consolas						" cursives eixiren tallades
endif

""""""""""""""""""""""""""""""""    INDENTS """"""""""""""""""""""""""""""""""""""""""""""""""""""
filetype indent plugin on					" Carrega configuracions de indents de vim74/indents
set autoindent 								" Quan no hi ha cap llengutge s'activa aco, crec

""""""""""""""""""""""""""""""""  CMD LINE	""""""""""""""""""""""""""""""""""""""""""""""""""""""
set showcmd									" Mostra la utlima ordre
set cmdheight=2								" La linia d'ordres te dos linies dalt

""""""""""""""""""""""""""""""""    UI 		""""""""""""""""""""""""""""""""""""""""""""""""""""""
set number									" Posa els numeros de linia
set cursorline								" Destaca la linia actual
set confirm									" Permet guardar quan es fa una ordre i hi ha canvis no guardats
set visualbell								" En comptes de so, fa parpadejos
"set t_vb= 									" Descomentar per que no hi hage avisos de cap tipus
set wildmenu								" Algo dun menu en les autocomplecions
set lazyredraw								" Evita redibuixos innecessaris
set showmatch								" Mostra el parentesi, claudator o clau que tanca 

""""""""""""""""""""""""""""""""  STATUSLINE 	""""""""""""""""""""""""""""""""""""""""""""""""""""""
set laststatus=2							" Mostra la barra de estatus
"set ruler									" Indica la fila i columna del cursor

""""""""""""""""""""""""""""""""  SCROLL 	""""""""""""""""""""""""""""""""""""""""""""""""""""""
if !&scrolloff								" Sempre mante una linia extra visible quan es fa scroll.
  set scrolloff=1
endif
if !&sidescrolloff
  set sidescrolloff=5
endif
set display+=lastline						" No ho tinc molt clar

""""""""""""""""""""""""""""""""    CERQUES """"""""""""""""""""""""""""""""""""""""""""""""""""""
set incsearch								" Activa la cerca incremental	            
set hlsearch								" Resalta les paraules buscades

set ignorecase								" Fa que les cerques ignoren majuscules/minuscules
set smartcase

""""""""""""""""""""""""""""""""    FOLDING	""""""""""""""""""""""""""""""""""""""""""""""""""""""
set foldenable
set foldlevelstart=10						" Obri molst nivells de folding
set foldnestmax=10 							" Maxim nivell de folding. Si ne necessites mes, mala senyal.

"Tria'n nomes un
"set foldmethod=indent
"set foldmethod=marker 
"set foldmethod=manual
"set foldmethod=expr
set foldmethod=syntax
"set foldmethod=diff

""""""""""""""""""""""""""""""""  MAPPINGS	""""""""""""""""""""""""""""""""""""""""""""""""""""""
nnoremap <C-L> :nohl<CR><C-L>				" Mapeja <C-L> per a desactivar el resaltat d'una cerca
nnoremap j gj								" Estes 2 opcions fan que els moviments no salten les 
											" linies grans que han fet warp lines
nnoremap k gk

""Mapejos per a entrenar i no gastar els cursors
inoremap  <Up>     <NOP>
inoremap  <Down>   <NOP>
inoremap  <Left>   <NOP>
inoremap  <Right>  <NOP>
noremap   <Up>     <NOP>
noremap   <Down>   <NOP>
noremap   <Left>   <NOP>
noremap   <Right>  <NOP>

""""""""""""""""""""""""""""""""  SYNTASTIC	""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Per a funcionar, el sistema ha de tindre els programes externs que fan el checkeig.
" El plugin syntastic per si no fa res de checkeig
" Sembla que cal activar-lo amb :Helptags
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*
let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0
Helptags

"""""""""""""""""""""""""""""	COMPLECIO DE COODI """"""""""""""
set omnifunc=syntaxcomplete#Complete

""""""""""""""""""          DESACTIVATS A PESAR DE RECOMANATS                  """"""""""""""""""""""""""""
" Stop certain movements from always going to the first character of a line.
" While this behaviour deviates from that of Vi, it does what most users
" coming from other editors would expect.
" set nostartofline

" Quickly time out on keycodes, but never time out on mappings
" set notimeout ttimeout ttimeoutlen=200

" Use <F11> to toggle between 'paste' and 'nopaste'
" set pastetoggle=<F11>


" Map Y to act like D and C, i.e. to yank until EOL, rather than act as yy,
" which is the default
" map Y y$

" Space open/closes folds
" nnoremap <space> za

" Canvia la manera d'anar a principi i final de linia. Desactiva els originals
"nnoremap B ^
"nnoremap E $
"nnoremap $ <nop>
"nnoremap ^ <nop>


"set diffexpr=MyDiff()
"function MyDiff()
"  let opt = '-a --binary '
"  if &diffopt =~ 'icase' | let opt = opt . '-i ' | endif
"  if &diffopt =~ 'iwhite' | let opt = opt . '-b ' | endif
"  let arg1 = v:fname_in
"  if arg1 =~ ' ' | let arg1 = '"' . arg1 . '"' | endif
"  let arg2 = v:fname_new
"  if arg2 =~ ' ' | let arg2 = '"' . arg2 . '"' | endif
"  let arg3 = v:fname_out
"  if arg3 =~ ' ' | let arg3 = '"' . arg3 . '"' | endif
"  let eq = ''
"  if $VIMRUNTIME =~ ' '
"    if &sh =~ '\<cmd'
"      let cmd = '""' . $VIMRUNTIME . '\diff"'
"      let eq = '"'
"    else
"      let cmd = substitute($VIMRUNTIME, ' ', '" ', '') . '\diff"'
"    endif
"  else
"    let cmd = $VIMRUNTIME . '\diff'
"  endif
"  silent execute '!' . cmd . ' ' . opt . arg1 . ' ' . arg2 . ' > ' . arg3 . eq
"endfunction

" Com no se molt be lo que es aixo del leader, no ho poso
" let mapleader="," 
