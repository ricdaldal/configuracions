##############################################################
#
#  SCRIPT BASH
#
##############################################################
#!/bin/bash
#  Autor          : enric
#  Data           : 30/03/2015
#  Ultima edicio  : 11/09/2016, enric
#  Descripcio     : Configura el vim instal.lat amb els estils meus
##############################################################
#  Proposit:
#   - Treballar de la mateixa manera amb tots els vim 
#  Requeriments:
#   - vim
#  Execucio:
#   - Com a usuari, una vegada. En l'institut, una vegada per sessió.
#  Notes:
#   - 
##############################################################

############################
### PRE CONFIGURACIO VIM ###
############################
which vim
if [ $? != "0" ] ; then
	echo "Sembla que vim no esta instalat. Avortant."
	exit 
fi

########################
### CONFIGURACIO VIM ###
########################
cp -R vim/vimfiles ~/.vim
cp vim/_vimrc ~/.vimrc

##########################
### PLUGIN VIM ECLIPSE ###
##########################
cp vim/_vrapperrc ~/.vrapperc


